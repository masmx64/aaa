
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.Text;

import org.junit.Before;
import org.junit.Test;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;


public class VCombinerTest
{

    ReduceDriver<VCityIDWritable, IntWritable, VCityIDWritable, IntWritable> comDriver;

    @Before
    public void setUp()
	{
        MapReduce.VCombiner reducer = new MapReduce.VCombiner();
        comDriver = ReduceDriver.newReduceDriver(reducer);
    }

    @Test
    public void testCombinerNormal() throws IOException
	{
		List<IntWritable> v0 = new ArrayList<IntWritable>();
		v0.add(new IntWritable(1));
		v0.add(new IntWritable(1));
		v0.add(new IntWritable(1));
		
		List<IntWritable> v1 = new ArrayList<IntWritable>();
		v1.add(new IntWritable(1));
		
		List<IntWritable> v2 = new ArrayList<IntWritable>();
		v2.add(new IntWritable(1));
		v2.add(new IntWritable(1));		
		
		comDriver.withInput(new VCityIDWritable(123), v0);
		comDriver.withInput(new VCityIDWritable(218), v1);
		comDriver.withInput(new VCityIDWritable(99), v2);
		
		comDriver.withOutput(new VCityIDWritable(123), new IntWritable(3));
		comDriver.withOutput(new VCityIDWritable(218), new IntWritable(1));
		comDriver.withOutput(new VCityIDWritable(99), new IntWritable(2));
		
		comDriver.runTest();		
    }
	
	
}